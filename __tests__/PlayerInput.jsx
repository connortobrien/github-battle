import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import { fireEvent, render, cleanup } from '@testing-library/react'

import PlayerInput from '../app/components/Battle/PlayerInput'

describe('<PlayerInput />', () => {
	afterEach(cleanup)

	it('renders the component', () => {
		const { asFragment } = render(<PlayerInput id="test" label="Test Label" onSubmit={() => null} />)
		expect(asFragment()).toMatchSnapshot()
	})

	it('sets the label from inputed props', () => {
		const labelText = 'Test Label'
		const { getByTestId } = render(<PlayerInput id="test" label={labelText} onSubmit={() => null} />)
		expect(getByTestId('username-label').textContent).toBe(labelText)
	})

	it('sets the input field', () => {
		const { getByTestId } = render(<PlayerInput id="test" label="Test Label" onSubmit={() => null} />)
		const input = getByTestId('username-input')
		expect(input.value).toBe('')
		fireEvent.change(input, { target: { value: 'swift-panda' } })
		expect(input.value).toBe('swift-panda')
	})
})
