import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import { render, cleanup } from '@testing-library/react'

import PlayerPreview from '../app/components/Battle/PlayerPreview'

describe('<PlayerPreview />', () => {
	afterEach(cleanup)

	it('renders the component', () => {
		const { asFragment } = render(
			<PlayerPreview
				avatar="avatar"
				username="swift-panda"
			>
				<h1>child</h1>
			</PlayerPreview>,
		)
		expect(asFragment()).toMatchSnapshot()
	})
})
