import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import { render, cleanup } from '@testing-library/react'

import Battle from '../app/components/Battle'

describe('<PlayerPreview />', () => {
	afterEach(cleanup)

	it('renders the component', () => {
		const { asFragment } = render(<Battle match={{ url: 'match_url' }} />)
		expect(asFragment()).toMatchSnapshot()
	})
})
