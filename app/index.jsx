import React from 'react'
import { createRoot } from 'react-dom'
import 'core-js/stable'
import 'regenerator-runtime/runtime'

import App from './components/App'

createRoot(document.getElementById('app')).render(<App />)
