import React from 'react'
import { NavLink } from 'react-router-dom'
import styled from 'styled-components'

const Navigation = styled.ul`
	display: flex;
	padding: 0;
`

const NavigationItem = styled.li`
	margin-right: 15px;
	list-style-type: none;

	a {
		text-decoration: none;
		color: #d0021b
	}

	& .active {
		font-weight: bold;
	}
`

const Nav = () => (
	<Navigation>
		<NavigationItem>
			<NavLink exact activeClassName="active" to="/">Home</NavLink>
		</NavigationItem>
		<NavigationItem>
			<NavLink activeClassName="active" to="/battle">Battle</NavLink>
		</NavigationItem>
		<NavigationItem>
			<NavLink activeClassName="active" to="/popular">Popular</NavLink>
		</NavigationItem>
	</Navigation>
)

export default Nav
