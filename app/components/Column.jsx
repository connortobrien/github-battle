import styled from 'styled-components'

const columnStyles = `
	display: flex;
	flex-direction: column;
	max-width: 500px;
	align-items: center;
`

export const Column = styled.div`
	${columnStyles}
`

export const ColumnForm = styled.form`
	${columnStyles}
`
