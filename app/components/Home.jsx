import React from 'react'
import styled from 'styled-components'

import { ButtonLink } from './Button'

const HomeContainer = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
`

const Home = () => (
	<HomeContainer>
		<h1>Github Battle: Battle your friends... and stuff.</h1>
		<ButtonLink href="/battle" to="/battle">Battle</ButtonLink>
	</HomeContainer>
)

export default Home
