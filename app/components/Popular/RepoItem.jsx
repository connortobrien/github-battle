import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const PopularItem = styled.li`
	margin: 20px;
	text-align: center;
`

const PopularRank = styled.div`
	font-size: 20px;
	margin: 10px;
`

const SpaceListItems = styled.ul`
	margin-bottom: 7px;
	padding: 0;
	list-style-type: none;
`

const Avatar = styled.img`
	width: 150px;
	border-radius: 50%
`

const ItemLink = styled.a`
	text-decoration: none;
	color: #d0021b
`

const RepoItem = ({
	index,
	name,
	owner: { login, avatar_url: avatarUrl },
	html_url: htmlUrl,
	stargazers_count: stargazersCount,
}) => (
	<PopularItem key={name}>
		<PopularRank>#{index + 1}</PopularRank>
		<SpaceListItems>
			<li>
				<Avatar src={avatarUrl} alt={`Avatar for ${login}`} />
			</li>
			<li><ItemLink href={htmlUrl}>{name}</ItemLink></li>
			<li>@{login}</li>
			<li>{stargazersCount} stars</li>
		</SpaceListItems>
	</PopularItem>
)

RepoItem.propTypes = {
	index: PropTypes.number.isRequired,
	name: PropTypes.string.isRequired,
	owner: PropTypes.shape({
		login: PropTypes.string.isRequired,
		avatar_url: PropTypes.string.isRequired,
	}).isRequired,
	html_url: PropTypes.string.isRequired,
	stargazers_count: PropTypes.number.isRequired,
}

export default RepoItem
