import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const UnsortedList = styled.ul`
	display: flex;
	flex-flow: row wrap;
	justify-content: center;
	padding: 0;
	list-style-type: none;

	& .selected {
		color: #d0021b;
	}
`

const ListItem = styled.li`
	margin: 10px;
	font-weight: bold;
	cursor: pointer;
`

const SelectLanguage = ({ selectedLanguage, onSelect }) => {
	const languages = ['All', 'Javascript', 'Ruby', 'Elixir', 'Java', 'Go', 'CSS', 'Python']
	const changeLanguage = (lang) => () => onSelect(lang)

	return (
		<UnsortedList>
			{languages.map((lang) => (
				<ListItem
					className={lang === selectedLanguage ? 'selected' : ''}
					key={lang}
					onClick={changeLanguage(lang)}
					onKeyPress={changeLanguage(lang)}
					role="presentation"
				>
					{lang}
				</ListItem>
			))}
		</UnsortedList>
	)
}

SelectLanguage.propTypes = {
	selectedLanguage: PropTypes.string.isRequired,
	onSelect: PropTypes.func.isRequired,
}

export default SelectLanguage
