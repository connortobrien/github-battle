import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { fetchPopularRepos } from '../../utils/api'
import RepoItem from './RepoItem'

const PopularList = styled.ul`
	display: flex;
	flex-wrap: wrap;
	justify-content: space-around;
	padding: 0;
	list-style-type: none;
`

const RepoGrid = ({ language }) => {
	const repos = fetchPopularRepos.read(language)

	return (
		<PopularList>
			{repos.map((repo, index) => (
				<RepoItem key={repo.name} {...repo} index={index} />
			))}
		</PopularList>
	)
}

RepoGrid.propTypes = {
	language: PropTypes.string,
}

RepoGrid.defaultProps = {
	language: 'All',
}

export default RepoGrid
