import React, { useState, Suspense } from 'react'
import RepoGrid from './RepoGrid'
import SelectLanguage from './SelectLanguage'
import Loading from '../Loading'

const Popular = () => {
	const [selectedLanguage, setLanguage] = useState('All')

	return (
		<div>
			<SelectLanguage
				selectedLanguage={selectedLanguage}
				onSelect={(lang) => setLanguage(lang)}
			/>
			<Suspense fallBack={<Loading />}>
				<RepoGrid language={selectedLanguage} />
			</Suspense>
		</div>
	)
}

export default Popular
