import React, { useRef, useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const LoadingText = styled.p`
	text-align: center;
	font-size: 35px;
`

const Loading = ({ text, speed }) => {
	const stopper = `${text}...`
	const intervalRef = useRef()
	const latestLoadingText = useRef()
	const [loadingText, setLoadingText] = useState(text)

	const updateLoadingText = () => {
		if (latestLoadingText.current === stopper) setLoadingText(text)
		else setLoadingText(`${latestLoadingText.current}.`)
	}
	useEffect(() => { latestLoadingText.current = loadingText })
	useEffect(() => {
		intervalRef.current = window.setInterval(updateLoadingText, speed)
		return () => window.clearInterval(intervalRef.current)
	}, [text, speed])

	return (
		<LoadingText>
			{loadingText}
		</LoadingText>
	)
}

Loading.propTypes = {
	text: PropTypes.string,
	speed: PropTypes.number,
}

Loading.defaultProps = {
	text: 'Loading',
	speed: 300,
}

export default Loading
