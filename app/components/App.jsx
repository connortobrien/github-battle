import React, { lazy, Suspense } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import styled from 'styled-components'

import GlobalStyle from './GlobalStyle'
import Home from './Home'
import Nav from './Nav'
import Loading from './Loading'

const AsyncBattle = lazy(() => import(/* webpackChunkName: "battle", webpackPrefetch: true */ './Battle'))
const AsyncResults = lazy(() => import(/* webpackChunkName: "results", webpackPrefetch: true */ './Results'))
const AsyncPopular = lazy(() => import(/* webpackChunkName: "popular", webpackPrefetch: true */ './Popular'))

const Container = styled.div`
	max-width: 1200px;
	margin: 0 auto;
`

const App = () => (
	<>
		<GlobalStyle />
		<BrowserRouter>
			<Container>
				<Nav />
				<Suspense fallback={<Loading />}>
					<Switch>
						<Route exact path="/" component={Home} />
						<Route exact path="/battle" component={AsyncBattle} />
						<Route path="/battle/results" component={AsyncResults} />
						<Route path="/popular" component={AsyncPopular} />
						<Route render={() => <p>Not Found</p>} />
					</Switch>
				</Suspense>
			</Container>
		</BrowserRouter>
	</>
)

export default App
