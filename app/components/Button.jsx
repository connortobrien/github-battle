import styled from 'styled-components'
import { Link } from 'react-router-dom'

const buttonStyles = `
	color: #e6e6e6;
	background: #0a0a0a;
	border: none;
	font-size: 16px;
	border-radius: 3px;
	width: 200px;
	text-align: center;
	text-decoration: none;
	display: block;
	padding: 7px 0;
	margin: 10px auto;

	&:active {
		transform: translateY(1px);
	}
	&:disabled {
		cursor: not-allowed;
		border: #0a0a0a;
		color: #0a0a0a;
		background: #e6e6e6;
	}
	&:hover:enabled {
		cursor: pointer;
		background: linear-gradient(#1a1a1a,#0a0a0a);
		color: #fff;
		text-decoration: none;
	}
`

export const Button = styled.button`
	${buttonStyles}
`

export const ButtonLink = styled(Link)`
	${buttonStyles}
`
