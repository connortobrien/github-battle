import styled from 'styled-components'

const Row = styled.div`
	display: flex;
	flex-flow: row wrap;
	justify-content: space-around;
`

export default Row
