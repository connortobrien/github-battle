import React, { Suspense } from 'react'
import PropTypes from 'prop-types'

import { ButtonLink } from '../Button'
import Row from '../Row'
import Player from './Player'
import Loading from '../Loading'
import { battle } from '../../utils/api'

const Results = ({ location: { search } }) => (
	<Suspense fallback={<Loading />}>
		<Players search={search} />
	</Suspense>
)

Results.propTypes = {
	location: PropTypes.shape({
		search: PropTypes.string.isRequired,
	}).isRequired,
}

export default Results

const Players = ({ search }) => {
	const results = battle.read(search)
	if (!results) {
		return (
			<div>
				<p>Looks like there was an error. Check that both users exist on Github.</p>
				<ButtonLink href="/battle" to="/battle">Reset</ButtonLink>
			</div>
		)
	}
	const [winner, loser] = results
	return (
		<Row>
			<Player
				label="Winner"
				score={winner.score}
				profile={winner.profile}
			/>
			<Player
				label="Loser"
				score={loser.score}
				profile={loser.profile}
			/>
		</Row>
	)
}

Players.propTypes = {
	search: PropTypes.string.isRequired,
}
