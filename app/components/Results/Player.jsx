import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import Profile from './Profile'

const Score = styled.h3`
	text-align: center;
`

const Header = styled.h1`
	text-align: center;
	font-size: 30px;
	font-weight: 200;
`

const Player = ({ label, score, profile }) => (
	<div>
		<Header>{label}</Header>
		<Score>Score: {score}</Score>
		<Profile info={profile} />
	</div>
)

Player.propTypes = {
	profile: PropTypes.shape({
		name: PropTypes.string,
		location: PropTypes.string,
		company: PropTypes.string,
		followers: PropTypes.number.isRequired,
		following: PropTypes.number.isRequired,
		public_repos: PropTypes.number.isRequired,
		blog: PropTypes.string,
	}).isRequired,
	label: PropTypes.string.isRequired,
	score: PropTypes.number.isRequired,
}

export default Player
