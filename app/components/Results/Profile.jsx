import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import PlayerPreview from '../Battle/PlayerPreview'

const SpaceListItems = styled.ul`
	padding: 0;
	list-style-type: none;
`

const Profile = (
	{
		info: {
			public_repos: publicRepos,
			avatar_url: avatarUrl,
			login, name, location, company, followers, following, blog,
		},
	},
) => (
	<PlayerPreview avatar={avatarUrl} username={login}>
		<SpaceListItems>
			{name && <li>{name}</li>}
			{location && <li>{location}</li>}
			{company && <li>{company}</li>}
			<li>Followers: {followers}</li>
			<li>Following: {following}</li>
			<li>Public Repos: {publicRepos}</li>
			{blog && <li><a href={blog}>{blog}</a></li>}
		</SpaceListItems>
	</PlayerPreview>
)

Profile.propTypes = {
	info: PropTypes.shape({
		avatar_url: PropTypes.string,
		login: PropTypes.string,
		name: PropTypes.string,
		location: PropTypes.string,
		company: PropTypes.string,
		followers: PropTypes.number.isRequired,
		following: PropTypes.number.isRequired,
		public_repos: PropTypes.number.isRequired,
		blog: PropTypes.string,
	}).isRequired,
}

export default Profile
