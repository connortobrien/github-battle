import React, { useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { Button } from '../Button'
import { ColumnForm } from '../Column'

const Header = styled.label`
	text-align: center;
	font-size: 30px;
	font-weight: 200;
`

const UsernameInput = styled.input`
	border-radius: 3px;
	margin: 10px 0;
	padding: 5px;
	border: 1px solid rgba(0, 0, 0, 0.43);
	font-size: 16px;
	width: 80%;
`

const PlayerInput = ({ label, onSubmit, id }) => {
	const [username, setUsername] = useState('')
	const handleChange = (event) => setUsername(event.target.value)
	const handleSubmit = (event) => {
		event.preventDefault()
		onSubmit(id, username)
	}

	return (
		<ColumnForm onSubmit={handleSubmit}>
			<Header data-testid="username-label" htmlFor="username">
				{label}
				<UsernameInput
					id="username"
					data-testid="username-input"
					placeholder="github username"
					type="text"
					autoComplete="off"
					value={username}
					onChange={handleChange}
				/>
			</Header>
			<Button type="submit" disabled={!username}>Submit</Button>
		</ColumnForm>
	)
}

PlayerInput.propTypes = {
	label: PropTypes.string.isRequired,
	onSubmit: PropTypes.func.isRequired,
	id: PropTypes.string.isRequired,
}

export default PlayerInput
