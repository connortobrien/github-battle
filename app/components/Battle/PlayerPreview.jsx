import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { Column } from '../Column'

const Avatar = styled.img`
	width: 150px;
	border-radius: 50%
`

const PlayerPreview = ({ avatar, children, username }) => (
	<div>
		<Column>
			<Avatar src={avatar} alt={`Avatar for ${username}`} />
			<h2 className="username">@{username}</h2>
		</Column>
		{children}
	</div>
)

PlayerPreview.propTypes = {
	avatar: PropTypes.string.isRequired,
	children: PropTypes.shape({
		type: PropTypes.string.isRequired,
	}).isRequired,
	username: PropTypes.string.isRequired,
}

export default PlayerPreview
