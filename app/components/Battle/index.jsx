import React, { useState } from 'react'
import PropTypes from 'prop-types'

import Row from '../Row'
import { Button, ButtonLink } from '../Button'
import PlayerPreview from './PlayerPreview'
import PlayerInput from './PlayerInput'

const Battle = ({ match }) => {
	const [playerOneName, setPlayerOneName] = useState('')
	const [playerTwoName, setPlayerTwoName] = useState('')

	const handleReset = (id) => () => {
		if (id === 'playerOne') setPlayerOneName('')
		else if (id === 'playerTwo') setPlayerTwoName('')
	}

	const handleSubmit = (id, username) => {
		if (id === 'playerOne') setPlayerOneName(username)
		else if (id === 'playerTwo') setPlayerTwoName(username)
	}

	return (
		<div>
			<Row>
				{!playerOneName && <PlayerInput id="playerOne" label="Player One" onSubmit={handleSubmit} />}
				{playerOneName !== '' && (
					<PlayerPreview avatar={`https://github.com/${playerOneName}.png?size=200`} username={playerOneName} id="playerOne">
						<Button type="button" onClick={handleReset('playerOne')}>Reset</Button>
					</PlayerPreview>
				)}
				{!playerTwoName && <PlayerInput id="playerTwo" label="Player Two" onSubmit={handleSubmit} />}
				{playerTwoName !== '' && (
					<PlayerPreview avatar={`https://github.com/${playerTwoName}.png?size=200`} username={playerTwoName} id="playerTwo">
						<Button type="button" onClick={handleReset('playerTwo')}>Reset</Button>
					</PlayerPreview>
				)}
			</Row>
			{playerOneName && playerTwoName && (
				<ButtonLink
					href={`${match.url}/results?playerOneName=${playerOneName}&playerTwoName=${playerTwoName}`}
					to={{
						pathname: `${match.url}/results`,
						search: `?playerOneName=${playerOneName}&playerTwoName=${playerTwoName}`,
					}}
				>
					Battle
				</ButtonLink>
			)}
		</div>
	)
}

Battle.propTypes = {
	match: PropTypes.shape({
		url: PropTypes.string.isRequired,
	}).isRequired,
}

export default Battle
