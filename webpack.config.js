const path = require('path')
const webpack = require('webpack')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

let env
try {
	env = require('./config.json') // eslint-disable-line import/no-unresolved, global-require
} catch (err) {
	env = null
}

module.exports = {
	mode: 'development',
	entry: './app/index.jsx',
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'index_bundle.js',
		publicPath: '/',
	},
	resolve: {
		extensions: ['.js', '.jsx'],
	},
	module: {
		rules: [
			{ test: /\.(jsx?)$/, use: 'babel-loader' },
		],
	},
	devServer: {
		historyApiFallback: true,
	},
	plugins: [
		new CleanWebpackPlugin(),
		new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: JSON.stringify('development'),
				CLIENT_ID: JSON.stringify(env.CLIENT_ID),
				CLIENT_SECRET: JSON.stringify(env.CLIENT_SECRET),
			},
		}),
		new HtmlWebpackPlugin({
			template: 'app/index.html',
		}),
	],
}
