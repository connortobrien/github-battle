const path = require('path')
const webpack = require('webpack')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

let env
try {
	env = require('./config.json') // eslint-disable-line import/no-unresolved, global-require
} catch (err) {
	env = null
}

module.exports = {
	mode: 'production',
	entry: path.resolve(__dirname, 'app/index.jsx'),
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: '[name].bundle.[hash:6].js',
		chunkFilename: '[name].bundle.[hash:6].js',
		publicPath: '/',
	},
	resolve: {
		extensions: ['.js', '.jsx'],
	},
	module: {
		rules: [
			{ test: /\.(jsx?)$/, use: 'babel-loader' },
		],
	},
	optimization: {
		splitChunks: {
			chunks: 'initial',
			cacheGroups: {
				vendors: {
					test: /node_modules/,
				},
			},
		},
	},
	plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: JSON.stringify('development'),
				CLIENT_ID: JSON.stringify(env.CLIENT_ID),
				CLIENT_SECRET: JSON.stringify(env.CLIENT_SECRET),
			},
		}),
		new CleanWebpackPlugin(),
		new HtmlWebpackPlugin({
			inject: true,
			template: path.resolve('app/index.html'),
		}),
	],
}
